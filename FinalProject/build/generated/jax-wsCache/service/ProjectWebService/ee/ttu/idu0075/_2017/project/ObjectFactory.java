
package ee.ttu.idu0075._2017.project;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.ttu.idu0075._2017.project package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPersonResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/project", "getPersonResponse");
    private final static QName _AddPersonResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/project", "addPersonResponse");
    private final static QName _GetProjectResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/project", "getProjectResponse");
    private final static QName _AddProjectResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/project", "addProjectResponse");
    private final static QName _GetProjectMemberListResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/project", "getProjectMemberListResponse");
    private final static QName _AddProjectMemberResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/project", "addProjectMemberResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.ttu.idu0075._2017.project
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPersonRequest }
     * 
     */
    public GetPersonRequest createGetPersonRequest() {
        return new GetPersonRequest();
    }

    /**
     * Create an instance of {@link PersonType }
     * 
     */
    public PersonType createPersonType() {
        return new PersonType();
    }

    /**
     * Create an instance of {@link AddPersonRequest }
     * 
     */
    public AddPersonRequest createAddPersonRequest() {
        return new AddPersonRequest();
    }

    /**
     * Create an instance of {@link GetPersonListRequest }
     * 
     */
    public GetPersonListRequest createGetPersonListRequest() {
        return new GetPersonListRequest();
    }

    /**
     * Create an instance of {@link GetPersonListResponse }
     * 
     */
    public GetPersonListResponse createGetPersonListResponse() {
        return new GetPersonListResponse();
    }

    /**
     * Create an instance of {@link GetProjectRequest }
     * 
     */
    public GetProjectRequest createGetProjectRequest() {
        return new GetProjectRequest();
    }

    /**
     * Create an instance of {@link ProjectType }
     * 
     */
    public ProjectType createProjectType() {
        return new ProjectType();
    }

    /**
     * Create an instance of {@link AddProjectRequest }
     * 
     */
    public AddProjectRequest createAddProjectRequest() {
        return new AddProjectRequest();
    }

    /**
     * Create an instance of {@link GetProjectListRequest }
     * 
     */
    public GetProjectListRequest createGetProjectListRequest() {
        return new GetProjectListRequest();
    }

    /**
     * Create an instance of {@link GetProjectListResponse }
     * 
     */
    public GetProjectListResponse createGetProjectListResponse() {
        return new GetProjectListResponse();
    }

    /**
     * Create an instance of {@link GetProjectMemberListRequest }
     * 
     */
    public GetProjectMemberListRequest createGetProjectMemberListRequest() {
        return new GetProjectMemberListRequest();
    }

    /**
     * Create an instance of {@link ProjectMemberListType }
     * 
     */
    public ProjectMemberListType createProjectMemberListType() {
        return new ProjectMemberListType();
    }

    /**
     * Create an instance of {@link AddProjectMemberRequest }
     * 
     */
    public AddProjectMemberRequest createAddProjectMemberRequest() {
        return new AddProjectMemberRequest();
    }

    /**
     * Create an instance of {@link ProjectMemberType }
     * 
     */
    public ProjectMemberType createProjectMemberType() {
        return new ProjectMemberType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/project", name = "getPersonResponse")
    public JAXBElement<PersonType> createGetPersonResponse(PersonType value) {
        return new JAXBElement<PersonType>(_GetPersonResponse_QNAME, PersonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/project", name = "addPersonResponse")
    public JAXBElement<PersonType> createAddPersonResponse(PersonType value) {
        return new JAXBElement<PersonType>(_AddPersonResponse_QNAME, PersonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProjectType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/project", name = "getProjectResponse")
    public JAXBElement<ProjectType> createGetProjectResponse(ProjectType value) {
        return new JAXBElement<ProjectType>(_GetProjectResponse_QNAME, ProjectType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProjectType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/project", name = "addProjectResponse")
    public JAXBElement<ProjectType> createAddProjectResponse(ProjectType value) {
        return new JAXBElement<ProjectType>(_AddProjectResponse_QNAME, ProjectType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProjectMemberListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/project", name = "getProjectMemberListResponse")
    public JAXBElement<ProjectMemberListType> createGetProjectMemberListResponse(ProjectMemberListType value) {
        return new JAXBElement<ProjectMemberListType>(_GetProjectMemberListResponse_QNAME, ProjectMemberListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProjectMemberType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/project", name = "addProjectMemberResponse")
    public JAXBElement<ProjectMemberType> createAddProjectMemberResponse(ProjectMemberType value) {
        return new JAXBElement<ProjectMemberType>(_AddProjectMemberResponse_QNAME, ProjectMemberType.class, null, value);
    }

}
