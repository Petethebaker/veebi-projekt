
package ee.ttu.idu0075.project;

import ee.ttu.idu0075._2017.project.AddPersonRequest;
import ee.ttu.idu0075._2017.project.GetPersonListRequest;
import ee.ttu.idu0075._2017.project.GetPersonListResponse;
import ee.ttu.idu0075._2017.project.GetPersonRequest;
import ee.ttu.idu0075._2017.project.PersonType;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

@Path("persons")
public class PersonsResource {

    @Context
    private UriInfo context;

    public PersonsResource() {
    }
    
    @GET
    @Produces("application/json")
    public GetPersonListResponse getPersonList(@QueryParam("token") String token) {
        ProjectWebService ws = new ProjectWebService();
        GetPersonListRequest request = new GetPersonListRequest();
        request.setToken(token);
        return ws.getPersonList(request);
    }
    
    @GET
    @Path("{id : \\d+}")
    @Produces("application/json")
    public PersonType getPerson(@PathParam("id") String id, @QueryParam("token") String token) {
        ProjectWebService ws = new ProjectWebService();
        GetPersonRequest request = new GetPersonRequest();
        request.setId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return ws.getPerson(request);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public PersonType addPerson(PersonType content, @QueryParam("token") String token, @QueryParam("requestID") BigInteger requestID) {
        ProjectWebService ws = new ProjectWebService();
        AddPersonRequest request = new AddPersonRequest();
        request.setRequestId(requestID);
        request.setFirstName(content.getFirstName());
        request.setLastName(content.getLastName());
        request.setToken(token);
        return ws.addPerson(request);
    }
}
