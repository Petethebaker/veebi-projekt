package ee.ttu.idu0075.project;

import ee.ttu.idu0075._2017.project.AddProjectMemberRequest;
import ee.ttu.idu0075._2017.project.AddProjectRequest;
import ee.ttu.idu0075._2017.project.GetProjectListRequest;
import ee.ttu.idu0075._2017.project.GetProjectListResponse;
import ee.ttu.idu0075._2017.project.GetProjectMemberListRequest;
import ee.ttu.idu0075._2017.project.GetProjectRequest;
import ee.ttu.idu0075._2017.project.ProjectMemberListType;
import ee.ttu.idu0075._2017.project.ProjectMemberType;
import ee.ttu.idu0075._2017.project.ProjectType;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;


@Path("projects")
public class ProjectsResource {

    @Context
    private UriInfo context;

    public ProjectsResource() {
    }
    
    @GET
    @Produces("application/json")
    public GetProjectListResponse getProjectList(@QueryParam("token") String token) {
        ProjectWebService ws = new ProjectWebService();
        GetProjectListRequest request = new GetProjectListRequest();
        request.setToken(token);
        return ws.getProjectList(request);
    }
    

    @GET
    @Path("{id : \\d+}")
    @Produces("application/json")
    public ProjectType getProject(@PathParam("id") String id, @QueryParam("token") String token) {
        ProjectWebService ws = new ProjectWebService();
        GetProjectRequest request = new GetProjectRequest();
        request.setId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return ws.getProject(request);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public ProjectType addProject(ProjectType content, @QueryParam("token") String token, @QueryParam("requestID") BigInteger requestID) {
        ProjectWebService ws = new ProjectWebService();
        AddProjectRequest request = new AddProjectRequest();
        request.setRequestId(requestID);
        request.setProjectName(content.getProjectName());
        request.setBeginDate(content.getBeginDate());
        request.setDueDate(content.getDueDate());
        request.setCustomerName(content.getCustomerName());
        request.setToken(token);
        return ws.addProject(request);
    }
    
    @GET
    @Path("{id : \\d+}/members")
    @Produces("application/json")
    public ProjectMemberListType getProjectMemberList(@PathParam("id") String id, @QueryParam("token") String token, @QueryParam("role") String role) {
        ProjectWebService ws = new ProjectWebService();
        GetProjectMemberListRequest request = new GetProjectMemberListRequest();
        request.setProjectId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setRole(role);
        request.setToken(token);
        return ws.getProjectMemberList(request);
    }
    
    @POST
    @Path("{id : \\d+}/addMember")
    @Consumes("application/json")
    @Produces("application/json")
    public ProjectMemberType addProjectMember(@PathParam("id") String id, @QueryParam("token") String token,
            @QueryParam("requestID") BigInteger requestID, @QueryParam("personID") String personID, @QueryParam("role") String role) {
        if (role == null || (!role.equals("juht") && !role.equals("arendaja") && !role.equals("testija"))) {
            return null;
        }
        ProjectWebService ws = new ProjectWebService();
        AddProjectMemberRequest request = new AddProjectMemberRequest();
        request.setPersonId(BigInteger.valueOf(Integer.parseInt(personID)));
        request.setProjectId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setRole(role);
        request.setToken(token);
        request.setRequestId(requestID);
        return ws.addProjectMember(request);
    }
}
