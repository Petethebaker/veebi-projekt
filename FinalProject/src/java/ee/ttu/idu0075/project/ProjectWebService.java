package ee.ttu.idu0075.project;

import com.sun.xml.ws.developer.SchemaValidation;
import ee.ttu.idu0075._2017.project.AddPersonRequest;
import ee.ttu.idu0075._2017.project.AddProjectMemberRequest;
import ee.ttu.idu0075._2017.project.AddProjectRequest;
import ee.ttu.idu0075._2017.project.GetPersonListRequest;
import ee.ttu.idu0075._2017.project.GetPersonListResponse;
import ee.ttu.idu0075._2017.project.GetPersonRequest;
import ee.ttu.idu0075._2017.project.GetProjectListRequest;
import ee.ttu.idu0075._2017.project.GetProjectListResponse;
import ee.ttu.idu0075._2017.project.GetProjectMemberListRequest;
import ee.ttu.idu0075._2017.project.GetProjectRequest;
import ee.ttu.idu0075._2017.project.PersonType;
import ee.ttu.idu0075._2017.project.ProjectMemberListType;
import ee.ttu.idu0075._2017.project.ProjectMemberType;
import ee.ttu.idu0075._2017.project.ProjectType;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.jws.WebService;

@SchemaValidation
@WebService(serviceName = "ProjectService", portName = "ProjectPort", endpointInterface = "ee.ttu.idu0075._2017.project.ProjectPortType", targetNamespace = "http://www.ttu.ee/idu0075/2017/project", wsdlLocation = "WEB-INF/wsdl/ProjectWebService/projectService.wsdl")
public class ProjectWebService {
    
    static int nextPersonID = 1;
    static List<PersonType> personList = new ArrayList<>();
    static Map<BigInteger, PersonType> addPersonRequests = new HashMap<>();
    
    static int nextProjectID = 1;
    static List<ProjectType> projectList = new ArrayList<>();
    static Map<BigInteger, ProjectType> addProjectRequests = new HashMap<>();
    
    static Map<BigInteger, ProjectMemberType> addProjectMemberRequests = new HashMap<>();
    
    public PersonType getPerson(GetPersonRequest parameter) {
        PersonType person = null;
        if (parameter.getToken().equalsIgnoreCase("salajane")) {
            for (PersonType p: personList) {
                if (p.getId().equals(parameter.getId())) {
                    person = p;
                    break;
                }
            }
        }
        return person;
    }

    public PersonType addPerson(AddPersonRequest parameter) {
        PersonType person = new PersonType();
        if (parameter.getToken().equalsIgnoreCase("salajane")) {
            System.out.println(parameter);
            if (addPersonRequests.containsKey(parameter.getRequestId())) {
                return addPersonRequests.get(parameter.getRequestId());
            }
            person.setFirstName(parameter.getFirstName());
            person.setLastName(parameter.getLastName());
            person.setId(BigInteger.valueOf(nextPersonID++));
            personList.add(person);
            addPersonRequests.put(parameter.getRequestId(), person);
        }
        return person;
    }

    public GetPersonListResponse getPersonList(GetPersonListRequest parameter) {
        GetPersonListResponse response = new GetPersonListResponse();
        if (parameter.getToken().equalsIgnoreCase("salajane")) {
            personList.forEach((p) -> {
                response.getPerson().add(p);
            });
        }
        return response;
    }

    public ProjectType getProject(GetProjectRequest parameter) {
        ProjectType project = null;
        if (parameter.getToken().equalsIgnoreCase("salajane")) {
            for (ProjectType p: projectList) {
                if (p.getId().equals(parameter.getId())) {
                    project = p;
                    break;
                }
            }
        }
        return project;
    }

    public ProjectType addProject(AddProjectRequest parameter) {
        ProjectType project = new ProjectType();
        if (parameter.getToken().equalsIgnoreCase("salajane")) {
            if (addProjectRequests.containsKey(parameter.getRequestId())) {
                return addProjectRequests.get(parameter.getRequestId());
            }
            project.setBeginDate(parameter.getBeginDate());
            project.setDueDate(parameter.getDueDate());
            project.setCustomerName(parameter.getCustomerName());
            project.setProjectName(parameter.getProjectName());
            project.setId(BigInteger.valueOf(nextProjectID++));
            project.setProjectMemberList(new ProjectMemberListType());
            projectList.add(project);
            addProjectRequests.put(parameter.getRequestId(), project);
        }
        return project;
    }

    public GetProjectListResponse getProjectList(GetProjectListRequest parameter) {
        GetProjectListResponse response = new GetProjectListResponse();
        if (parameter.getToken().equalsIgnoreCase("salajane")) {
            projectList.forEach((p) -> {
                response.getProject().add(p);
            });
            if (parameter.getBeginDate() != null) {
                response.getProject().removeIf(p -> !p.getBeginDate().equals(parameter.getBeginDate()));
            }
            if (parameter.getDueDate() != null) {
                response.getProject().removeIf(p -> !p.getDueDate().equals(parameter.getDueDate()));
            }
            if (parameter.getCustomerName() != null) {
                response.getProject().removeIf(p -> !p.getCustomerName().equals(parameter.getCustomerName()));
            }
        }
        return response;
    }

    public ProjectMemberListType getProjectMemberList(GetProjectMemberListRequest parameter) {
        if (parameter.getToken().equalsIgnoreCase("salajane")) {
            for (ProjectType p: projectList) {
                if (p.getId().equals(parameter.getProjectId())) {
                    if (parameter.getRole() != null) {
                        ProjectMemberListType response = new ProjectMemberListType();
                        List<ProjectMemberType> members = p.getProjectMemberList().getProjectMember();
                        for (ProjectMemberType m: members) {
                            if (m.getRole().equals(parameter.getRole())) {
                                response.getProjectMember().add(m);
                            }
                        }
                        return response;
                    }
                    return p.getProjectMemberList();
                }
            }
        }
        return null;
    }

    public ProjectMemberType addProjectMember(AddProjectMemberRequest parameter) {
        ProjectMemberType member = new ProjectMemberType();
        if (parameter.getToken().equalsIgnoreCase("salajane")) {
            if (addProjectMemberRequests.containsKey(parameter.getRequestId())) {
                return addProjectMemberRequests.get(parameter.getRequestId());
            }
            PersonType person = null;
            ProjectType project = null;
            for (PersonType p: personList) {
                if (p.getId().equals(parameter.getPersonId())) {
                    person = p;
                    break;
                }
            }
            for (ProjectType p: projectList) {
                if (p.getId().equals(parameter.getProjectId())) {
                    project = p;
                    break;
                }
            }
            member.setPerson(person);
            member.setRole(parameter.getRole());
            project.getProjectMemberList().getProjectMember().add(member);
            addProjectMemberRequests.put(parameter.getRequestId(), member);
        }
        return member;
    }
    
}
