package finalprojecttestapplication;

import ee.ttu.idu0075._2017.project.AddPersonRequest;
import ee.ttu.idu0075._2017.project.GetPersonRequest;
import ee.ttu.idu0075._2017.project.PersonType;
import ee.ttu.idu0075._2017.project.ProjectPortType;
import ee.ttu.idu0075._2017.project.ProjectService;
import java.math.BigInteger;


public class FinalProjectTestApplication {

    public static void main(String[] args) {
        AddPersonRequest addRequest = new AddPersonRequest();
        addRequest.setFirstName("Priit");
        addRequest.setLastName("Tohver");
        addRequest.setToken("salajane");
        addRequest.setRequestId(BigInteger.valueOf(1000));
        addPerson(addRequest);
        
        GetPersonRequest getRequest = new GetPersonRequest();
        getRequest.setId(BigInteger.ONE);
        getRequest.setToken("salajane");
        getPerson(getRequest);
    }

    private static PersonType addPerson(AddPersonRequest parameter) {
        ProjectService service = new ProjectService();
        ProjectPortType port = service.getProjectPort();
        return port.addPerson(parameter);
    }

    private static PersonType getPerson(GetPersonRequest parameter) {
        ProjectService service = new ProjectService();
        ProjectPortType port = service.getProjectPort();
        return port.getPerson(parameter);
    }
    
}
